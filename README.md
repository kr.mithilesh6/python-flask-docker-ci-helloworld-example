# A Python(FLASK)/Docker/CI HelloWorld example
A „Hello World!” sample written in Python using Flask app running on Docker.
This example demonstrates:

* A simple Python application using Flask
* Dockerfile to generate a Image containg the Flask app.

## Running the example locally

You can clone this project and run the docker build locally:

```shell
docker build -t flask-sample-one:latest .
docker run -d -p 5000:5000 flask-sample-one
```

# How to deploy applications using Containers with GitLab CI/CD

## Introduction

In this project, we show how you can leverage the power of [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
to build a container using [Docker](https://www.docker.com/) or [Podman](https://podman.io/) and deploy it to a [Container Registry](https://registry.bio.di.uminho.pt/) or [DockerHub](https://hub.docker.com/).

We assume that you already have a GitLab account on [gitLab.bio.di.uminho.pt](http://gitlab.bio.di.uminho.pt/), and that you know the basic usage of Git, [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) and [Docker](https://www.docker.com/).

You'll use this project as example to test Gitlab CI using Docker/Buildah and our internal Container Registry/Dockerhub.

## Create the Project

In this specific case, we'll use a simple Python with Flask app, but it could be any application compiled using dockerfile. 
For this article you'll use the app that can be cloned from our example
project:

1. Log in to your GitLab account
1. Create a new project by selecting **Import project from > Repo by URL**
1. Add the following URL:

   ```plaintext
   https://gitlab.bio.di.uminho.pt/tutorials/python-flask-docker-ci-helloworld-example.git
   ```
1. Click **Create project**

This application is nothing more than python file with a Flask web controller that prints "Flask: Hello World from Docker".

The project structure is really simple, and you should consider these two resources:

- `Dockerfile`: project dockerfile to generate a container image using Docker or Buildah
- `/app/src/app.py`: main of our application
- `/app/requirements.txt`: python package requirements for pip installation inside container image

### Configure GitLab CI/CD 

Now it's time we set up [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) to automatically build and deploy the docker image!

GitLab CI/CD uses a file in the root of the repository, named `.gitlab-ci.yml`, to read the definitions for jobs
that will be executed by the configured runners. You can read more about this file in the [GitLab Documentation](https://docs.gitlab.com/ee/ci/yaml/README.md).

**By default Gitlab CI/CD will use the user credentials to push docker images to our internal registry.**


However some people prefers to deploy to **DockerHub** (not mandatory), which requires credentials set up in deployment variables.
Navigate to your project's **Settings > CI/CD > Environment variables** page
and add the following ones (replace them with your current values, of course):

**!!! Only set this variables if you will upload the image to DockerHub!!**

- **CI_REGISTRY_USER**: `dockerhubuser` (DockerHub Username)
- **CI_REGISTRY_PASSWORD**: `dockerhubpassword` (DockerHub Password)
- **CI_REGISTRY**: `docker.io` (By default DockerHub registy url  is docker.io, it must be set)
- **CI_REGISTRY_IMAGE**: `dockerhubuser/projectname` (Registy image tag from dockerhub repostory created after this [step](https://docs.docker.com/docker-hub/repos/))

#### Build container image using Docker and Deploy to our internal Registry

Docker can compile container images using DockerFile configurations.
In this section, we will explain how to build container imagens and deploy to registry using Gitlab CI.
Now it's time to define jobs in `.gitlab-ci.yml` and push it to the repository:

```yaml
image: docker:latest
services: 
  - docker:dind

build:
  stage: build
  script:
    - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .

deploy:
   stage: deploy
   script:
    - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest
   only:
    - master
```
The runner uses the latest [Docker image](https://hub.docker.com/_/docker/),
which contains all of the tools and dependencies needed to manage the project
and to run the jobs.

Both `build` and `deploy` jobs leverage the `docker` command to compile the container and to deploy it to the registry configurated.
The deployment occurs only if we're pushing or merging to `master` branch, so that the development versions are tested but not published in the registry.

If the deployment has been successful, the deploy job log will output: 

```plaintext
Job succeeded
```

Done! Now you have all the changes in the GitLab repository, and a pipeline has already been started for this commit. In the **Pipelines** tab you can see what's happening.
If the deployment has been successful, the deploy job log will output:

Yay! You did it! Checking in repository will confirm that you have a new image available.
You can look in the `Packages & Registries > Container Registry`, if you have deploed to our internal Registry.

#### Build container image using Buildah to our internal Registry (experimental - It has few compatibilty issues with our runnables)

As alternative to docker, Buildah makes available the possability to build OCI images ([see more info](https://buildah.io/blogs/2017/11/02/getting-started-with-buildah.html)).

```yaml
image: quay.io/buildah/stable:latest

build:
  stage: build
  script:
    - buildah login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - buildah pull $CI_REGISTRY_IMAGE:latest || true
    - buildah bud --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .

deploy:
   stage: deploy
   script:
    - buildah login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - buildah push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - buildah push $CI_REGISTRY_IMAGE:latest
   only:
    - master
```
The runner uses the latest [Buildah image](https://hub.docker.com/_/buildah/),
which contains all of the tools and dependencies needed to manage the project
and to run the jobs.

Both `build` and `deploy` jobs leverage the `buildah` command to compile the container and to deploy it to the registry configurated.
The deployment occurs only if we're pushing or merging to `master` branch, so that the development versions are tested but not published in the registry.

If the deployment has been successful, the deploy job log will output: 

```plaintext
Job succeeded
```

Done! Now you have all the changes in the GitLab repository, and a pipeline has already been started for this commit. In the **Pipelines** tab you can see what's happening.
If the deployment has been successful, the deploy job log will output:

Yay! You did it! Checking in repository will confirm that you have a new image available.
You can look in the `Packages & Registries > Container Registry`, if you have deploed to our internal Registry.

#### Build container image using Docker/Buildah to Dockerhub

Since we are only changing the repository where you will push de compiled image, you only need to set the variables described above in the Configure GitLab CI/CD section and you must make a single change to your `.gitlab-ci.yml` file.

For **Docker** configurations the line:
```yaml
    - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
```
Should be replaced with:

```yaml
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
```
For **Buildah** configurations the line:
```yaml
    - buildah login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
```
Should be replaced with:

```yaml
    - buildah login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
```
Done. Now check In the **Pipelines** tab if the job is succeeded and go to DockerHub repository to find your new image.
