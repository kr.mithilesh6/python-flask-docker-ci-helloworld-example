FROM python:latest

MAINTAINER BYOSYSTEMS group <kr.mithilesh6@gmail.com>

ENV APPPATH /opt/myflaskapp
COPY . $APPPATH
WORKDIR $APPPATH/app
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["src/app.py"]
